<?php
/**
 * Created by nicolasey.
 * Date: 02/04/16
 * Time: 21:07
 */

namespace App\Mailers;

use Illuminate\Contracts\Mail\Mailer;

class BaseMailer {
    protected $mailer;
    protected $from;

    protected $to;
    protected $view;
    protected $data = [];

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->from = env("MAIL_USERNAME");
    }

    protected function deliver()
    {
        $this->mailer->send($this->view, $this->data, function($message) {
            $message->from($this->from, "StarWars Open Rift")
                ->to($this->to)
            ;
        });
    }
}